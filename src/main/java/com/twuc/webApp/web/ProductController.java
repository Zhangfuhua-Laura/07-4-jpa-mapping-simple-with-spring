package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@Controller
@RequestMapping("/api")
public class ProductController {
    @Autowired
    private ProductRepository productRepository;

    @PostMapping("/products")
    ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest request) {
        Product product = productRepository.save(new Product(request.getName(), request.getPrice(), request.getUnit()));
        return ResponseEntity
                .status(201)
                .header("location", String.format("http://localhost/api/products/%d", product.getId()))
                .build();
    }

    @GetMapping("/products/{productId}")
    ResponseEntity getProduct(@PathVariable Long productId) {
        System.out.println("productId" + productId);
        Optional<Product> product = productRepository.findById(productId);
        if (!product.isPresent())
            return ResponseEntity.status(404).build();
        else
            return ResponseEntity.status(200).body(product.get());
    }

}